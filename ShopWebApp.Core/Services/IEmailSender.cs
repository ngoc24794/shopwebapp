﻿// ==++==
// 
//   Copyright (c) Shop Web App.  All rights reserved.
//   Author: Nguyen Van Ngoc
// 
// ==--==

using System.Threading.Tasks;

namespace ShopWebApp.Core.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
