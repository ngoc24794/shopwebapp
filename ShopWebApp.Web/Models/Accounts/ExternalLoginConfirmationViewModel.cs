﻿// ==++==
// 
//   Copyright (c) Shop Web App.  All rights reserved.
//   Author: Nguyen Van Ngoc
// 
// ==--==

using System.ComponentModel.DataAnnotations;

namespace ShopWebApp.Web.Models.Accounts
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
