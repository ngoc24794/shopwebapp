﻿// ==++==
// 
//   Copyright (c) eShop Web App.  All rights reserved.
//   Author: Nguyen Van Ngoc
// 
// ==--==

using Microsoft.AspNetCore.Identity;

namespace ShopWebApp.Infra.Models.Identity
{
    public class ApplicationUser : IdentityUser
    {
    }
}
