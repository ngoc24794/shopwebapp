﻿// ==++==
// 
//   Copyright (c) Shop Web App.  All rights reserved.
//   Author: Nguyen Van Ngoc
// 
// ==--==

using System.ComponentModel.DataAnnotations;

namespace ShopWebApp.Infra.Models.AccountViewModels
{
    public class ExternalLoginViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
